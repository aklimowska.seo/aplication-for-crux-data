
// eporting data from CrUX api //
const CrUXApiUtil = {};
CrUXApiUtil.API_ENDPOINT = `https://chromeuxreport.googleapis.com/v1/records:queryRecord?key=AIzaSyBinR62SRPCPzaJuTsSSfh__djks5Com5w`;
CrUXApiUtil.query = function (requestBody) {
  if (CrUXApiUtil.API_KEY == ["YOUR_API_KEY"]) {
    throw 'Replace "YOUR_API_KEY" with your private CrUX API key. Get a key at https://goo.gle/crux-api-key.';
  }
  return fetch(CrUXApiUtil.API_ENDPOINT, {
    method: 'POST',
    body: JSON.stringify(requestBody)
  }).then(response => response.json()).then(response => {
    if (response.error) {
      return Promise.reject(response);
    }
    return response;
  });
};

// list of Urls, which data will be export//
var orgins = [
  'https://www.onet.pl',
  'https://kobieta.onet.pl',
  'https://wiadomosci.onet.pl',
  'https://podroze.onet.pl',
  'https://gotowanie.onet.pl',
  'https://sport.onet.pl',
  'https://facet.onet.pl',
  'https://kultura.onet.pl',
  'https://plejada.pl',
  'https://magia.onet.pl',
  'https://zapytaj.onet.pl',
  'https://pogoda.onet.pl',
  'https://programtv.onet.pl',
]

let Resultdata = [];

//function, which filter data to get one object for onet URL with MOBILE and DESKTOP data//
function filterData() {
  for (var i = 0; i < orgins.length; i++) {
    const filterQuery = {
      orgin: orgins[i],
    }
    const filteredData = Resultdata.filter(
      item => Object.keys(filterQuery).every(key => item[key] === filterQuery[key])
    );
    console.log(filteredData)
  }
}


//function, which export data and push it to the REsultdata table//
function pushDataToObject(response) {
  const formFactor = response.record.key.formFactor;
  const orginName = response.record.key.origin;
  const Lcp = response.record.metrics.largest_contentful_paint.histogram[0].density.toFixed(2) * 100;
  const Fid = response.record.metrics.first_input_delay.histogram[0].density.toFixed(2) * 100;
  const Cls = response.record.metrics.cumulative_layout_shift.histogram[0].density.toFixed(2) * 100;


  Resultdata.push({
    formFactor: formFactor,
    orgin: orginName,
    lcp: Lcp,
    fid: Fid,
    cls: Cls
  })
}

// function, which export data from api. First from the PHONE formfactor and safe it in the Resultdata. Next, the same steps for DESKTOP fromfactor. then the data for MOBILE and DESKTOP for same Url should be filter and display in aplication.//
function getResultofCWV() {
  for (var i = 0; i < orgins.length; i++) {
    var orgin = orgins[i];
    console.log(orgin)
    CrUXApiUtil.query({
      formFactor: 'PHONE',
      origin: orgin,
    }).then(response => {
      // console.log(response);
      pushDataToObject(response);
    }).then(CrUXApiUtil.query({
      formFactor: 'DESKTOP',
      origin: orgin,
    }).then(response => {
      pushDataToObject(response);
      console.log(Resultdata);
      filterData()
    })).catch(response => {
      console.error(response);
    })
  }
}


//funtion, which allow to export custom list of url and export the data for it - this function is in progess//
function customListOfUrl() {
  var orgins = [];
  console.log(orgins)
  const infoBox = document.getElementById('infoBox');
  document.getElementById('buttons').setAttribute('style', 'display:none');
  const inputBox = document.createElement('div');
  inputBox.setAttribute('class','inputBox');
  const inputLabel = document.createElement('label');
  const input = document.createElement('input');
  // input.setAttribute('type','text');

  infoBox.appendChild(inputBox);
  inputBox.appendChild(inputLabel);
  inputBox.appendChild(input);

}

